const cron = require("node-cron");
const cleanUpShedules = require("../src/services/cleanUpSchedulesServices");

// Agendamento da limpeza
cron.schedule("*/30 * * * * *", async() => {
    try {
        await cleanUpShedules();
        console.log("Limpeza automática executada!");
    } catch (error) {
        console.error("Erro ao executar limpeza automática!");
    };  // Fim do catch
});  // Fim do cron.schedule