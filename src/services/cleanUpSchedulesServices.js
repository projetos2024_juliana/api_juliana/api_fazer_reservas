const connect = require("../db/connect");

async function cleanUpSchedulesServices(){
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate() - 7); // Definir a data para 7 dias atrás.
    const formattedDate = currentDate.toISOString().split('T')[0]; // Formata a data para YYYY-MM-DD (padrão americano).

    const query = `DELETE FROM schedule WHERE dateEnd < ?`;
    const values = [formattedDate];

    return new Promise((resolve, reject) => {
        connect.query(query, values, function(err, results) {
            if (err) {  
                console.error("Erro Mysql", err);
                return reject(new Error("Erro ao limpar agendamentos"));
            }

            console.log("Agendamento antigos apagados!");
            resolve("Agendamentos antigos limpos com sucesso!");
        });  // Fim da connect.query
    });  // Fim do return
};  // Fim da function principal cleanUpScheduleServices

module.exports = cleanUpSchedulesServices;
